import io.restassured.response.Response;
import org.json.JSONArray;
import org.junit.Assert;
import org.junit.Test;

import static io.restassured.RestAssured.get;

public class TestAPI {
    @Test
    public void testREST() {
        Response resp = get("http://restcountries.eu/rest/v1/name/russia");

        JSONArray jsonResponse = new JSONArray(resp.asString());

        String capital = jsonResponse.getJSONObject(0).getString("capital");

        System.out.println(jsonResponse);

        Assert.assertEquals(capital, "Moscow");
    }
}
